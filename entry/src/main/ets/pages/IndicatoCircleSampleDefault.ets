/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CircleIndicator, CircleModel } from '@ohos/circleindicator'

@Extend(Tabs) function indicator (onTouchListener) {
  .barWidth(0)
  .onTouch(onTouchListener)
}

@Entry
@Component
struct IndicatoCircleScale {
  private text= ["CUPCAKE", "DONUT", "ECLAIR", "GINGERBREAD",
  "HONEYCOMB", "ICE_CREAM_SANDWICH", "JELLY_BEAN", "KITKAT", "LOLLIPOP", "M", "NOUGAT"]
  private controller: TabsController = new TabsController()
  @State model: CircleModel= new CircleModel(this.controller)
  @State model2: CircleModel= new CircleModel(this.controller)
  @State model1: CircleModel= new CircleModel(this.controller)
  @State model3: CircleModel= new CircleModel(this.controller)
  @State itemIndex: number= 0
  @State count: number = 0

  @Builder SquareText(index:number) {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Text(this.text[index]).fontSize(40).textAlign(TextAlign.Center)
    }.height("100%").width("100%")
    .padding(15)
  }

  @Builder TabContentSimple(index:number) {
    TabContent() {
      this.SquareText(index)
    }
  }

  aboutToAppear() {
    this.model
      .setUnselectedColor("#f5eff5")
      .setWidth(230) //总体宽度
      .setHeight(50) //总体高度
      .setRadius(15)
      .setMargin(5)
      .setSnap(true) //移动轨迹

    this.model2 //旋转的矩形
      .setUnselectedColor("#817c81") //矩形背景色
      .setWidth(230) //总体宽度
      .setHeight(50) //总体高度
      .setRectangle(true) //是否矩形
      .setItemWidth(10) //矩形宽度
      .setItemHeight(10) //矩形高度
      .setRotate(true) //开启旋转

    this.model3 //设置移动轨迹
      .setMargin(5) //间隔
      .setRadius(15) //圆形弧度
      .setWidth(230)
      .setHeight(50)
      .setBorderLines(true) //是否显示变框线
      .setStrokeColor("#336699") //边框线颜色
      .setBackgroundColor("#ddaacc") //总体背景

    this.count = this.text.length
  }

  build() {
    Column() {
      Tabs({ index: this.itemIndex, controller: this.controller }) {
        this.TabContentSimple(0)
        this.TabContentSimple(1)
        this.TabContentSimple(2)
        this.TabContentSimple(3)
        this.TabContentSimple(4)
        this.TabContentSimple(5)
        this.TabContentSimple(6)
        this.TabContentSimple(7)
        this.TabContentSimple(8)
        this.TabContentSimple(9)
        this.TabContentSimple(10)
        this.TabContentSimple(11)
      }
      .width("100%").height("70%")
      .onChange((index)=>{
        this.itemIndex = index
      })
      .indicator((event: TouchEvent) => {
        this.model.notifyTouch(event, this.itemIndex)
      })

      CircleIndicator({ itemIndex: $itemIndex, model: this.model, count: $count })
      CircleIndicator({ itemIndex: $itemIndex, model: this.model2, count: $count })
      CircleIndicator({ itemIndex: $itemIndex, model: this.model3, count: $count })
    }
    .width('100%')
    .height('100%')
    .backgroundColor(Color.Pink)
  }
}