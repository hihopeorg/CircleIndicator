/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MagicScrollTabsIndicator, MagicScrollTabsModel, MagicTitleMode, MagicCursorType, MagicDividerWidthMode
} from '@ohos/circleindicator'
//import curves  from '@ohos.curves'

@Extend(Tabs) function indicator (onTouchListener) {
  .barWidth(0)
  .width("100%").height("80%")
  .onTouch(onTouchListener)
}

@Entry
@Component
struct MagicSampleDefault {
  private initData: Array<string>
    = ["CUPCAKE", "DONUT", "ECLAIR", "GINGERBREAD", "HONEYCOMB", "ICE_CREAM_SANDWICH", "JELLY_BEAN", "KITKAT", "LOLLIPOP", "M", "NOUGAT"]
  private controller: TabsController = new TabsController()
  @State model8: MagicScrollTabsModel = new MagicScrollTabsModel(this.controller)
  @State index: number = 0

  @Builder SquareText(index: number) {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Text(this.initData[index]).fontSize(20).textAlign(TextAlign.Center)
    }.height("100%").width("100%")
    .padding(15)
  }

  @Builder TabContentSimple(index: number) {
    TabContent() {
      this.SquareText(index)
    }
  }

  aboutToAppear() {

    this.model8
      .setTitles(this.initData)
      .setUnselectedTextSize(12)
      .setSelectedTextSize(16)
      .setBackgroundColor(0xfafafa)
      .setSelectedTextColor(0x000000)
      .setUnselectedTextColor(0x616161)
      .setTitleMode(MagicTitleMode.COLOR_GRADIENT)
      .setMaxCircleRadius(6)
      .setMinCircleRadius(4)
      .setHorizontalPadding(10)
      .setCursorType(MagicCursorType.BEZIER)
      .setIndicatorColors([0xff4a42, 0xfcde64, 0x73e8f4, 0x76b0ff, 0xc683fe])
    //      .setLeftCurve(curves.cubicBezier(0.7, 0.15, 0.7, 0.15))
    //      .setRightCurve(curves.cubicBezier(0.15, 0.7, 0.15, 0.7))

  }

  build() {
    Column({ space: 5 }) {
      MagicScrollTabsIndicator({ index: $index, model: this.model8, titles: this.initData })
      Tabs({ index: this.index, controller: this.controller }) {
        this.TabContentSimple(0)
        this.TabContentSimple(1)
        this.TabContentSimple(2)
        this.TabContentSimple(3)
        this.TabContentSimple(4)
        this.TabContentSimple(5)
        this.TabContentSimple(6)
        this.TabContentSimple(7)
        this.TabContentSimple(8)
        this.TabContentSimple(9)
        this.TabContentSimple(10)
      }
      .onChange((index)=>{
        this.index = index
      })
      .indicator((event: TouchEvent) => {
        let index = this.index
        this.model8.notifyTouch(event, index)
      })
    }
    .width('100%')
    .height('100%')
    .backgroundColor(0xeeeeee)
  }
}