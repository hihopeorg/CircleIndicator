/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CapsuleFixTabsIndicator, CapsuleFixTabsModel } from '@ohos/circleindicator'

@Entry
@Component
struct CapsuleFixTabsIndicator_page {
  private controller: TabsController = new TabsController()
  @State model: CapsuleFixTabsModel = new CapsuleFixTabsModel(this.controller)
  @State index: number= 0

  aboutToAppear() {
    this.model
      .setSelectedTextColor('#ffffff')
      .setUnselectedTextColor('#d7674e')
      .setFillColor('#a7271c')
      .setWidth(250)
      .setUnselectedTextSize(17)
      .setSelectedTextSize(17)
      .setBackgroundColor('#dddddd')
  }

  build() {
    Column() {
      CapsuleFixTabsIndicator({ itemIndex: $index, model: this.model, titles: ['KITKAT', 'NOUGAT', 'DONUT'] })

      Tabs({ index: this.index, controller: this.controller }) {
        TabContent() {
          Image($r('app.media.p1'))
        }

        TabContent() {
          Image($r('app.media.p2'))
        }

        TabContent() {
          Image($r('app.media.p3'))
        }

      }
      .width("100%")
      .height("100%")
      .onChange((index)=>{
        this.index = index
      })
      .barWidth(0)
      .barHeight(0)
      .onTouch((event: TouchEvent) => {
        this.model.notifyTouch(event, this.index)
      })
      .backgroundColor('#aaaaaa')
    }
    .width('100%')
    .height('100%')
  }
}