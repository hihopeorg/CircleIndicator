/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {TitleIndicator} from '@ohos/circleindicator'
import {TitleModel} from '@ohos/circleindicator'
import prompt from '@system.prompt'

@Entry
@Component
struct TitlesSampleDefault {
  tabsController: TabsController = new TabsController()
  @State
  model: TitleModel = new TitleModel(this.tabsController);
  titles: string[] = ["This", "Is", "A", "Test"]
  @State
  index: number = 0;

  @Builder
  makeTabContent(index: number) {
    TabContent() {
      Stack() {
        Text(this.calcText(this.titles[index])).fontSize(68).fontColor(0x777777).margin({ right: 8, left: 8 })
      }.width('100%').height('100%')
    }.width('100%').height('100%')
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      TitleIndicator({ titles: this.titles, itemIndex: $index, model: this.model })
      Tabs({ controller: this.tabsController }) {
        this.makeTabContent(0)
        this.makeTabContent(1)
        this.makeTabContent(2)
        this.makeTabContent(3)
      }
      .width('100%')
      .height("100%")
      .onChange((index)=>{
        this.index = index
      })
      .barHeight(0)
      .onTouch((event: TouchEvent) => {
        this.model.notifyTouch(event, this.index)
      })
    }.width('100%').height('100%')
  }

  aboutToAppear() {
    this.model
      .setBackgroundColor(0xffcccccc)
      .setOnCenterItemClickListener((index: number) => {
        prompt.showToast({ message: "You clicked the center title!" })
      })
      .setChangeListener((index: number) => {
        prompt.showToast({ message: "Changed to page " + index })
      });
  }

  calcText(text: string): string {
    return (text + " ").repeat(20)
  }
}