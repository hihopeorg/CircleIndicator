## v1.1.1

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.1.0

- 名称由@ohos/circle-indicator修改为@ohos/circleindicator
- 旧的包@ohos/circle-indicator已不维护，请使用新包@ohos/circleindicator

## v1.0.0

- 已实现功能
  1. 可与ark UI接口提供的分页类容器搭配使用
  2、指示分页类容器的页面数量与当前页
  3、滑动点击响应
  4、提供多种UI风格
  5、CenterView功能
  6、固定位置的Tabs风格指示器
  7、可滑动的的Tabs风格指示器
  8、角标功能
  9、滑块锚点设置
  10、固定首列功能

- 未支持功能
  1、用户自定义角标风格、title布局
