/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SpringScrollTabsModel } from '../models/SpringScrollTabsModel'

@Component
struct SpringScrollTabsIndicator {
  @State model: SpringScrollTabsModel = new SpringScrollTabsModel(null)
  @Link @Watch("itemIndexChange") itemIndex: number
  @State titles: Array<string | number> = []
  private scroller: Scroller = new Scroller()
  private lastIndex: number = 0
  private startX
  @State indicatorOffset: number = 0
  private timeoutId = -1;

  itemIndexChange() {
    let itemIndex = this.itemIndex
    this.model.getTabsController().changeIndex(itemIndex)
    let that = this
    that.scroller.scrollTo({
      xOffset: ((-px2vp(this.model.getViewWidth()) / 2)) + that.scrollExtra(itemIndex) + this.computeTabsWidth() / 2,
      yOffset: 0,
      animation: { duration: 300, curve: Curve.Ease }
    })
    let index = this.lastIndex
    that.indicatorOffset = -vp2px(that.scrollExtra(index) - that.scrollExtra(itemIndex))
    let intervalID = setInterval(function () {
      that.indicatorOffset += vp2px(that.scrollExtra(index) - that.scrollExtra(itemIndex)) / 10
    }, 15);
    setTimeout(function () {
      that.indicatorOffset = 0
      clearInterval(intervalID)
    }, 150);

    this.lastIndex = itemIndex
    if (this.model.getChangeListener()) {
      this.model.getChangeListener()(this.itemIndex)
    }
  }

  @Builder TabItem(data: number | string, index: number) {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Stack() {
        Row() {
          Stack() {
            Text(data + "")
              .fontSize(this.comFontSize(index, this.itemIndex))
              .fontColor(this.comFontColor(index, this.itemIndex))
              .width(this.computeTabsWidth())
              .textAlign(TextAlign.Center)
          }
        }
        .border({ radius: 30 })
        .zIndex(10)
      }
    }
    .height("100%")
    .onClick(() => {
      this.model.setClickChange(true)
      this.itemIndex = index
    })
  }

  aboutToAppear() {
    this.model.setOnPageTouchListener(this.onIndicatorTouch.bind(this))
  }

  build() {
    Stack({ alignContent: Alignment.Start }) {
      Scroll(this.scroller) {
        Stack({ alignContent: Alignment.Start }) {
          Row() {
            ForEach(this.titles.map((item1, index1) => {
              return { i: index1, data: item1 };
            }), (item) => {
              this.TabItem(item.data, item.i)
            }, item => JSON.stringify(item))
          }.zIndex(10)

          if (this.model.getHeight()) {
            Stack({ alignContent: Alignment.Center }) {
              Path().width("100%").height("100%").commands(
                "M" + vp2px(this.computeWidth(this.itemIndex) - this.model.getHeight() / 2) + " " + vp2px(this.model.getHeight() / 2) +
                " L" + vp2px(this.computeWidth(this.itemIndex) - this.model.getHeight() / 2) + " " + vp2px(this.model.getHeight() / 2 - this.comBelzierCircle(this.itemIndex, false) / 2) +
                " Q" + vp2px(this.computeWidth(this.itemIndex) / 2) + " " + vp2px(this.model.getHeight() / 2) + " " + vp2px(this.model.getHeight() / 2) + " " + vp2px(this.model.getHeight() / 2 - this.comBelzierCircle(this.itemIndex, true) / 2) +
                " L" + vp2px(this.model.getHeight() / 2) + " " + vp2px(this.model.getHeight() / 2 + this.comBelzierCircle(this.itemIndex, true) / 2) +
                " Q" + vp2px(this.computeWidth(this.itemIndex) / 2) + " " + vp2px(this.model.getHeight() / 2) + " " + vp2px(this.computeWidth(this.itemIndex) - this.model.getHeight() / 2) + " " + vp2px(this.model.getHeight() / 2 + this.comBelzierCircle(this.itemIndex, false) / 2) + " Z")
                .fill(this.model.getSpringColor())

              Circle({
                width: this.comBelzierCircle(this.itemIndex, true),
                height: this.comBelzierCircle(this.itemIndex, true)
              })
                .markAnchor({ x: (this.computeWidth(this.itemIndex) - this.model.getHeight()) / 2, y: 0 })
                .fill(this.model.getSpringColor())

              Circle({
                width: this.comBelzierCircle(this.itemIndex, false),
                height: this.comBelzierCircle(this.itemIndex, false)
              })
                .markAnchor({ x: -(this.computeWidth(this.itemIndex) - this.model.getHeight()) / 2, y: 0 })
                .fill(this.model.getSpringColor())
            }
            .height(this.model.getHeight())
            .width(this.computeWidth(this.itemIndex))
            .offset({
              y: 0,
              x: (this.scrollExtra(this.itemIndex)) - (this.computeWidth(this.itemIndex)) / 2 + this.computeTabsWidth() / 2

            })
          }
        }
      }
      .scrollable(ScrollDirection.Horizontal)
      .align(Alignment.Start)
      .width('100%')
      .height('100%')
      .backgroundColor(this.model.getBackgroundColor())
    }.height(this.model.getHeight()).width(this.model.getWidth())
  }

  onIndicatorTouch(event: TouchEvent, itemIndex: number) {
    let startX = this.startX
    let offset
    let that = this
    if (event.type === TouchType.Down) {
      this.startX = event.touches[0].x
    }
    if (event.type === TouchType.Move) {
      offset = event.touches[0].x - startX
      if ((offset >= 0 && itemIndex > 0) || (offset <= 0 && itemIndex < this.titles.length - 1)) {
        this.indicatorOffset = offset
      }

      let width = 0
      this.scroller.scrollTo({
        xOffset: (-px2vp(this.model.getViewWidth()) / 2) + this.scrollExtra(itemIndex) + width + this.computeTabsWidth() / 2,
        yOffset: 0
      })
    }
    if (event.type === TouchType.Up) {
      this.model.getTabsController().changeIndex(itemIndex)
      offset = 0
    }
    this.indicatorOffset = offset
    this.itemIndex = itemIndex
  }

  computeTabsWidth() {
    let tabWidth = 0
    if (this.titles.length >= 5) {
      tabWidth = px2vp(this.model.getViewWidth() / 5)
    } else if (this.titles.length > 0 && this.titles.length < 5) {
      tabWidth = px2vp(this.model.getViewWidth() / this.titles.length)
    }
    return tabWidth
  }

  scrollExtra(index) {
    let sum = 0
    for (var i = 0; i < index; i++) {
      sum += this.computeTabsWidth()
    }
    if (this.indicatorOffset > 0 && index > 0) {
      sum -= this.indicatorOffset / this.model.getViewWidth() * (2 * this.computeTabsWidth()) / 2

    } else if (this.indicatorOffset < 0 && index < this.titles.length - 1) {
      sum -= this.indicatorOffset / this.model.getViewWidth() * (2 * this.computeTabsWidth()) / 2
    }
    return sum
  }

  computeWidth(itemIndex: number, onOffset: boolean = false) {
    let width = 0
    width = this.model.getHeight()
    if (this.indicatorOffset > 0 && itemIndex > 0) {
      width = (1 / 2 - Math.abs(this.indicatorOffset / this.model.getViewWidth() - 1 / 2)) * 2 * this.computeTabsWidth() + this.model.getHeight()
    } else if (this.indicatorOffset < 0 && itemIndex < this.titles.length - 1) {
      width = (1 / 2 - Math.abs(this.indicatorOffset / this.model.getViewWidth() + 1 / 2)) * 2 * this.computeTabsWidth() + this.model.getHeight()
    }
    return width
  }

  comBelzierCircle(itemIndex, isLeft) {
    let radiusOffset = Math.abs(this.model.getMaxRadiusPercent() - this.model.getMinRadiusPercent())
    if (itemIndex > 0 && this.indicatorOffset > 0) {
      if (isLeft) {
        return ((this.indicatorOffset / this.model.getViewWidth()) * radiusOffset) * this.model.getHeight() + this.model.getMinRadiusPercent() * this.model.getHeight()
      } else {
        return this.model.getMaxRadiusPercent() * this.model.getHeight() - (this.indicatorOffset / this.model.getViewWidth() * radiusOffset) * this.model.getHeight() //圆角系数   2
      }
    } else if (itemIndex < this.titles.length - 1 && this.indicatorOffset < 0) {
      if (isLeft) {
        return this.model.getMaxRadiusPercent() * this.model.getHeight() - (-this.indicatorOffset / this.model.getViewWidth() * radiusOffset) * this.model.getHeight() //圆角系数   2
      } else {
        return ((-this.indicatorOffset / this.model.getViewWidth()) * radiusOffset) * this.model.getHeight() + this.model.getMinRadiusPercent() * this.model.getHeight()
      }
    }
    return this.model.getMaxRadiusPercent() * this.model.getHeight()
  }

  comFontColor(index, itemIndex) {
    if (this.model.getSelectedTextColor()) {
      if (index == itemIndex) {
        if ((itemIndex > 0 && this.indicatorOffset > 0) || itemIndex < this.titles.length - 1 && this.indicatorOffset < 0) {
          return this.comHalfFontColor(Math.abs(this.indicatorOffset) / this.model.getViewWidth())
        }
        return this.model.getSelectedTextColor()
      } else if (itemIndex > 0 && index == itemIndex - 1 && this.indicatorOffset > 0) {
        return this.comHalfFontColor(1 - Math.abs(this.indicatorOffset) / this.model.getViewWidth())
      } else if (itemIndex < this.titles.length - 1 && index == itemIndex + 1 && this.indicatorOffset < 0) {
        return this.comHalfFontColor(1 - Math.abs(this.indicatorOffset) / this.model.getViewWidth())
      }
    }
    return this.model.getUnselectTextColor()
  }

  comHalfFontColor(offset) {
    let selectedTextColor = this.model.getSelectedTextColor()
      .substring(this.model.getSelectedTextColor().indexOf("#") + 1)
    let textColor = this.model.getUnselectTextColor().substring(this.model.getUnselectTextColor().indexOf("#") + 1)
    let halfFontColor
    halfFontColor = "#"
    for (var i = 0; i < 3; i++) {
      let a = Math.floor(parseInt("0x" + selectedTextColor.substring(i * 2, i * 2 + 2)) * (1 - offset) + parseInt("0x" + textColor.substring(i * 2, i * 2 + 2)) * offset)
        .toString(16)
      halfFontColor += a.length == 2 ? a : "0" + a
    }
    return halfFontColor
  }

  comFontSize(index, itemIndex) {
    if (this.model.getSelectedTextSize()) {
      if (index == itemIndex) {
        if (((itemIndex > 0 && this.indicatorOffset > 0) || itemIndex < this.titles.length - 1 && this.indicatorOffset < 0)) {
          return Math.abs(this.indicatorOffset) / this.model.getViewWidth() * this.model.getUnselectTextSize() + (1 - Math.abs(this.indicatorOffset) / this.model.getViewWidth()) * this.model.getSelectedTextSize()
        }
        return this.model.getSelectedTextSize()
      } else if (itemIndex > 0 && index == itemIndex - 1 && this.indicatorOffset > 0) {
        return Math.abs(this.indicatorOffset) / this.model.getViewWidth() * this.model.getSelectedTextSize() + (1 - Math.abs(this.indicatorOffset) / this.model.getViewWidth()) * this.model.getUnselectTextSize()
      } else if (itemIndex < this.titles.length - 1 && index == itemIndex + 1 && this.indicatorOffset < 0) {
        return Math.abs(this.indicatorOffset) / this.model.getViewWidth() * this.model.getSelectedTextSize() + (1 - Math.abs(this.indicatorOffset) / this.model.getViewWidth()) * this.model.getUnselectTextSize()
      }
    }
    return this.model.getUnselectTextSize()
  }
}


export default SpringScrollTabsIndicator