/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ColorGradient} from '../utils/ColorGradient'
import { CapsuleFixTabsModel } from '../models/CapsuleFixTabsModel'

@Component
struct CapsuleFixTabsIndicator {
  @State model: CapsuleFixTabsModel = new CapsuleFixTabsModel(null)
  @Link itemIndex: number
  @State titles: string[] = []
  private tabs: Array<any> = []
  private colorsUtilsToS: any = null
  private colorsUtilsToN: any = null
  private startX = 0
  private isMove: number = 0
  @State indicatorOffset: number = 0

  aboutToAppear() {
    this.model.setOnPageTouchListener(this.onIndicatorTouch.bind(this))

    this.colorsUtilsToS = new ColorGradient(this.model.getUnselectedTextColor(), this.model.getSelectedTextColor(), 100)
    this.colorsUtilsToN = new ColorGradient(this.model.getSelectedTextColor(), this.model.getUnselectedTextColor(), 100)

    for (var i = 0; i < this.titles.length; i++) {
      let item = {
        index: i,
        fontColor: this.model.getSelectedTextColor(),
        fontSize: this.model.getSelectedTextSize(),
        text: this.titles[i]
      }
      this.tabs.push(item)
    }
  }

  private dealWithFont(offset): number {
    if (!offset) offset = 0
    /**
     * 滑动结束
     */
    if (this.isMove == 0) {
      console.log('滑动结束');
      for (var i = 0; i < this.tabs.length; i++) {
        if (offset == i) {
          this.tabs[i].fontSize = this.model.getSelectedTextSize()
          this.tabs[i].fontColor = this.model.getSelectedTextColor()
        } else {
          this.tabs[i].fontSize = this.model.getUnselectedTextSize()
          this.tabs[i].fontColor = this.model.getUnselectedTextColor()
        }
      }
    }
    /**
     * 向左滑动
     */
    else if (this.isMove == 1) {
      if (offset <= 0) return 0
      console.log('向左滑动');
      let nextPage = Math.floor(offset)
      let distance = 1 - parseFloat((offset - Math.floor(offset)).toFixed(2))
      // 处理 文字大小 渐变
      let fontSizeOffset = (this.model.getSelectedTextSize() - this.model.getUnselectedTextSize()) * distance
      this.tabs[nextPage].fontSize = this.model.getUnselectedTextSize() + fontSizeOffset
      this.tabs[nextPage + 1].fontSize = this.model.getSelectedTextSize() - fontSizeOffset

      // 处理 文字颜色 渐变
      let fontColorDistance = (distance * 100).toFixed(0) // 偏移量 0 - 99
      this.tabs[nextPage].fontColor = this.colorsUtilsToS.getColorByFraction(fontColorDistance)
      this.tabs[nextPage + 1].fontColor = this.colorsUtilsToN.getColorByFraction(fontColorDistance)
    }
    /**
     * 向右滑动
     */
    else if (this.isMove == 2) {
      if (offset >= this.tabs.length - 1) return 0
      console.log('向右滑动');
      let nextPage = Math.ceil(offset)
      let distance = parseFloat((offset - Math.floor(offset)).toFixed(2)) // 偏移量 0.00 - 1.00
      // 处理 文字大小 渐变
      let fontSizeOffset = (this.model.getSelectedTextSize() - this.model.getUnselectedTextSize()) * distance
      this.tabs[nextPage].fontSize = this.model.getUnselectedTextSize() + fontSizeOffset
      this.tabs[nextPage - 1].fontSize = this.model.getSelectedTextSize() - fontSizeOffset

      // 处理 文字颜色 渐变
      let fontColorDistance = (distance * 100).toFixed(0) // 偏移量 0 - 99
      this.tabs[nextPage].fontColor = this.colorsUtilsToS.getColorByFraction(fontColorDistance)
      this.tabs[nextPage - 1].fontColor = this.colorsUtilsToN.getColorByFraction(fontColorDistance)
    }
    return 0
  }

  build() {
    Stack() {
      Stack({ alignContent: Alignment.Start }) {
        Text('').fontSize(this.dealWithFont(this.itemIndex - this.indicatorOffset / 1080))
        // 滑块
        Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
          Text('')
            .border({
              width: this.model.getBorderWidth(),
              color: this.model.getFillColor(),
              radius: this.model.getRadius(),
              style: BorderStyle.Solid
            })
            .height('100%')
            .width('100%')
            .backgroundColor(this.model.getFillColor())
        }
        .width(this.model.getWidth() / this.tabs.length)
        .height('100%')
        .padding({
          top: this.model.getVerticalPadding(), right: this.model.getHorizontalPadding(), bottom: this.model.getVerticalPadding(), left: this.model.getHorizontalPadding()
        })
        .offset({
          x: this.model.isAnimation() ? ((this.model.getWidth() / this.tabs.length + 1) * (this.itemIndex - this.indicatorOffset / 1080)) : ((this.model.getWidth() / this.tabs.length) * (this.itemIndex)),
          y: 0 })

        // tab项
        Row() {
          ForEach(this.tabs, (item) => {
            Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
              Text(item.text)
                .fontSize(this.itemIndex === item.index ? this.model.getUnselectedTextSize() : this.model.getSelectedTextSize())
                .fontColor(this.itemIndex === item.index ? this.model.getSelectedTextColor() : this.model.getUnselectedTextColor())
            }
            .onClick(() => {
              this.model.getTabsController().changeIndex(item.index)
              this.itemIndex = item.index
              if (this.model.getClickListener()) {
                this.model.getClickListener()(item.index)
              }
            })
            .width(this.model.getWidth() / this.tabs.length)
            .height('100%')
          }, item => JSON.stringify(item))
        }
        .border({
          width: this.model.getBorderWidth(),
          color: this.model.getFillColor(),
          radius: this.model.getRadius(),
          style: BorderStyle.Solid
        })
      }
    }
    .height(this.model.getHeight())
    .width('100%')
    .backgroundColor(this.model.getBackgroundColor())
  }

  onIndicatorTouch(event: TouchEvent, currentIndex: number) {
    let startX = this.startX
    let offset
    if (event.type === TouchType.Down) {
      this.startX = event.touches[0].x
    }
    if (event.type === TouchType.Move) {
      offset = event.touches[0].x - startX
      if (offset <= 0) {
        this.isMove = 2
      } else {
        this.isMove = 1
      }
      if ((offset >= 0 && currentIndex > 0) || (offset <= 0 && currentIndex < this.tabs.length - 1)) {
        this.indicatorOffset = offset
      }
    }
    if (event.type === TouchType.Up) {
      this.model.getTabsController().changeIndex(currentIndex)
      this.itemIndex = currentIndex
      offset = 0
      this.indicatorOffset = offset
      this.isMove = 0
      if (this.model.getChangeListener()) {
        this.model.getChangeListener()(currentIndex)
      }
    }
  }
}

export default CapsuleFixTabsIndicator