/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {evaluateTextHeight} from '../../utils/UiUtil'

@Component
struct MeasurableText {
  scroller: Scroller = new Scroller()
  @State @Watch("onTextChange")
  model: MeasurableText.Model = new MeasurableText.Model();
  @Prop @Watch("onTextChange")
  text: string
  private measured: boolean = false;
  @State
  textWidth: number = 0;
  @State
  textHeight: number = 0

  build() {
    Stack() {
      Scroll(this.scroller) {
        Row() {
          Flex().width("100%").height("100%")
          Stack() {
            Text(this.text)
              .fontSize(this.model.getFontSize())
              .fontWeight(this.model.getFontWeight())
              .fontColor(this.model.getTextColor())
              .fontFamily(this.model.getFontFamily())
              .opacity(this.model.getCenterOffset())
            Text(this.text)
              .fontSize(this.model.getFontSize())
              .fontWeight(this.model.getFontWeight())
              .fontColor(this.model.getSelectedColor())
              .fontFamily(this.model.getFontFamily())
              .opacity(1 - this.model.getCenterOffset())
          }
        }
      }
      .enabled(false)
      .scrollable(ScrollDirection.Horizontal)
      .height(evaluateTextHeight(this.model.getFontSize()))
      .width(this.textWidth)
      .backgroundColor(this.model.getBackgroundColor())
      .onScroll((xOffset: number, yOffset: number) => {
        if (!this.measured) {
          this.scroller.scrollTo({
            xOffset: '100%', yOffset: 0
          });
        }
      })
      .onScrollEnd(() => {
        if (this.textWidth !== this.scroller.currentOffset().xOffset) {
          this.textWidth = this.scroller.currentOffset().xOffset;
          this.measured = true;
          if (this.model.getMeasuredListener() !== undefined) {
            this.model.getMeasuredListener()(this.model.key, this.textWidth, evaluateTextHeight(this.model.getFontSize()))
          }
        }
      })
      .scrollBar(BarState.Off)
    }.enabled(false)
  }

  onTextChange() {
    this.scroller.scrollTo({
      xOffset: 360, yOffset: 0, //xOffset设为100%只有第一次有效，之后只能设一个极大坐标来滑到新的终点
    });
  }
}

namespace MeasurableText {
  export class Model {
    fontSize: number= 15;
    fontWeight: FontWeight = FontWeight.Normal;
    textColor: ResourceColor = 0xff000000;
    selectedColor: ResourceColor = 0xff000000;
    backgroundColor: ResourceColor = "#00ffffff";
    fontFamily: string = "sans-serif"
    centerOffset: number = 0;
    measuredListener: (key: any, width: number, height: number) => void;
    key: any = undefined

    setFontSize(fontSize: number): Model{
      this.fontSize = fontSize;
      return this;
    }

    getFontSize(): number{
      return this.fontSize;
    }

    setFontWeight(fontWeight: FontWeight): Model{
      this.fontWeight = fontWeight;
      return this;
    }

    getFontWeight(): FontWeight{
      return this.fontWeight;
    }

    setTextColor(textColor: ResourceColor): Model {
      this.textColor = textColor;
      return this;
    }

    getTextColor(): ResourceColor {
      return this.textColor;
    }

    setSelectedColor(selectedColor: ResourceColor): Model {
      this.selectedColor = selectedColor;
      return this;
    }

    getSelectedColor(): ResourceColor {
      return this.selectedColor;
    }

    setBackgroundColor(backgroundColor: ResourceColor): Model{
      this.backgroundColor = backgroundColor;
      return this;
    }

    getBackgroundColor(): ResourceColor{
      return this.backgroundColor;
    }

    setFontFamily(fontFamily: string): Model {
      this.fontFamily = fontFamily;
      return this;
    }

    getFontFamily(): string {
      return this.fontFamily;
    }

    setCenterOffset(centerOffset: number): Model {
      this.centerOffset = Math.max(Math.min(centerOffset, 1), 0);
      return this;
    }

    getCenterOffset() {
      return this.centerOffset;
    }

    setMeasuredListener(measuredListener: (key: any, width: number, height: number) => void,key:object): Model{
      this.measuredListener = measuredListener;
      this.key = key;
      return this;
    }

    getMeasuredListener(): (key: any, width: number, height: number) => void{
      return this.measuredListener;
    }
  }
}

export default MeasurableText;