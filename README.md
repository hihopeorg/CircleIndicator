# CircleIndicator

## 简介
CircleIndicator是一款UI组件库，为Tabs/Swiper容器提供了多种自定义风格的指示器

## 效果展示：

圆形指示器:

<img src="./gifs/circle.gif"/>

长条指示器:

<img src="./gifs/line.gif"/>

横幅指示器:

<img src="./gifs/banner.gif"/>

三角指示器:

<img src="./gifs/triangle.gif"/>

图标指示器:

<img src="./gifs/icon.gif"/>

携带中央视图的Tabs指示器:

<img src="./gifs/center.gif"/>

固定位置Tabs指示器:

<img src="./gifs/fix.gif"/>

固定位置Tabs指示器（胶囊风格）:

<img src="./gifs/capsule.gif"/>

固定位置Tabs指示器（携带角标）:

<img src="./gifs/badge.gif"/>

可滑动Tabs指示器:

<img src="./gifs/scroll.gif"/>

可滑动Tabs指示器(水滴滑块):

<img src="./gifs/spring.gif"/>

可滑动Tabs指示器(首列固定):

<img src="./gifs/pin.gif"/>

titles指示器:

<img src="./gifs/title.gif"/>

## 下载安装

````
npm install @ohos/circleindicator --save
````

OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。



## 使用说明

提供多种Indicator，使用方法类似，以TriangleIndicator为例

1. 初始化：实例化TabsController和对应的Model 对象, 并添加number类型成员以记录当前页下标

 ```
private controller: TabsController = new TabsController()
@State model: TriangularModel = new TriangularModel(this.controller)
@State itemIndex: number = 0
 ```

2. 属性设置：通过Model类对象设置UI属性来自定义所需风格，也可以添加所需的回调

 ```
aboutToAppear() {
  this.model
    .setReverse(true)
    .setLineHeight(4)
    .setTriangleHeight(10)
    .setLineColor("#e94220")
    .setBackgroundColor("#eeeeee")
    .setChangeListener((itemIndex) => {
      console.info("change page to " + this.data[itemIndex])
    })
}
 ```

3. 界面绘制：在Tabs组件旁放置Indicator组件，注意需要关闭原生的bar。并监听Tabs组件的touch事件，通知给Model类，以统一处理滑动逻辑

 ```
build() {
  Column() {
    TriangularIndicator({ itemIndex: $itemIndex, count: this.data.length, model: this.model })
    Tabs({ index: this.itemIndex, controller: this.controller }) {
      ……
    }
    .barWidth(0)
    .onTouch((event: TouchEvent) => {
      this.model.notifyTouch(event, this.itemIndex)
    })
  }.padding({ top: 40 })
  .backgroundColor("#eeeeee")
}
 ```

4. 若长度相关的自定义属性需要传入float资源,则需要在MainAbility.ts或app.ts文件(取决于entry为stage模型还是FA模型)中调用 `initResourceManager()`方法
以便组件内部进行Resource资源值的相关计算
 ```
import {initResourceManager} from '@ohos/circleindicator'

export default class MainAbility extends Ability {
    async onCreate(want, launchParam) {
        console.log("[Demo] MainAbility onCreate")
        globalThis.abilityWant = want;
        await initResourceManager(this.context)
    }
 ```

更多详细用法请参考开源库sample页面的实现

## 接口说明
`model: TriangularModel = new TriangularModel(this.controller) // (TriangularModel可替换为所需的Model类)`

1. 设置指示器高度

   `model.setWidth(width: Length)`
    注意: 1.长宽等Length型属性暂不支持百分比写法 2.当前OpenHarmony 的UI计算对无单位的float.json资源和无单位的ets变量值处理逻辑不同,前者会当做px处理,后者则为vp,未避免歧义,当传入$r('app.float.[id]')形式的参数时建议在定义资源处加上单位(px/vp/fp)
2. 设置指示器宽度

   `model.setHeight(height: Length)`
3. 设置指示器背景色

   `setBackgroundColor(backgroundColor: ResourceColor)`
4. 触摸事件拦截

   `model.notifyTouch(event: TouchEvent, index: number)`
5. 设置页面切换监听

   `model.setChangeListener(callback: (index: number) => void)`
6. 设置指示器总条数(属性)

   `@State count: number`
7. 初始化指示器当前index(属性)

   `@Link itemIndex: number`

## 约束与限制

在下述版本验证通过：

- DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## 目录结构

````
|---- CircleIndicator
|     |---- entry  # 示例代码文件夹
|     |---- CircleIndicator  # CircleIndicator库文件夹
|           |----src
|                 |----main
|                       |----ets
|                             |----components
|                                   |----CircleIndicator.ets #指示器的核心实现
|                                   |----utils #工具类，辅助UI的绘制
|           |---- index.ets  # 对外接口
|     |---- README.md  # 安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/CircleIndicator/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/CircleIndicator/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/CircleIndicator/blob/master/LICENSE) ，请自由地享受和参与开源。